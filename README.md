# Desafio - PHP

O objetivo desse teste é o desenvolvimento de uma API REST usando PHP.

Não há restrição nenhuma em relação a frameworks, fica a seu critério.

## Escopo

Imagine um cenário em que vamos criar uma API para o gerenciamento de cursos o desafio é desenvolver os seguintes endpoints:

### GET - /courses

Retorna todos os cursos da base de dados.

### POST - /courses

Cria um novo curso.

### PUT - /courses/{id}

Atualiza um curso existente.

### DELETE - /courses/{id}

Deleta um curso existente.

### Atributos do curso
  - Nome;
  - Descrição;
  - Valor;
  - Pago: se é um curso pago ou não;
  - Tipo: o tipo do curso de acordo com o perfil do empreendedor;

## Recomendações

  - Faça commits regularmente no seu projeto;
  - Mantenha o código organizado;
  - Explique o que é preciso para o projeto funcionar;
  - Os tipos possíveis são: p1, p2 e p3. Sendo que é possível que um curso possa pertencer a mais de um tipo;









